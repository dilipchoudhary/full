from django.db import models

# Create your models here.

class Student(models.Model):
    name = models.CharField(max_length=128, blank=True)
    age = models.SmallIntegerField(null=True, blank=True)

    def __str__(self) -> str:
        return self.name