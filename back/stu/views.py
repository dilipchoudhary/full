from django.http import JsonResponse
from django.shortcuts import render
from rest_framework.generics import ListAPIView, CreateAPIView
from .models import Student

from .serializers import StudentSerializer

# Create your views here.


class StudentListView(ListAPIView):
    serializer_class = StudentSerializer
    queryset = Student.objects.all()

class StudentCreateView(CreateAPIView):
    serializer_class = StudentSerializer
