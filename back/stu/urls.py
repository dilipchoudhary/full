from django.urls import path

from .views import StudentListView, StudentCreateView

app_name = 'stu'

urlpatterns = [
    path('student/', StudentListView.as_view(), name='student-list'),
    path('student-create/', StudentCreateView.as_view(), name='student-list')
]