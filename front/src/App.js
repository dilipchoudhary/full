import './App.css';
import {useState, useEffect } from 'react';
import axios from 'axios';
function App() {
  const [students,  setStudents] = useState([])  
  const [studentForm, setStudentForm] = useState({"name":"", "age":0})
  // const [student, setStudent] = useState({"name":"", "age":0})

  useEffect(()=>{
    getAllStudents()
  }, [])

  function getAllStudents(){
    axios({
      method:'GET',
      url:'http://127.0.0.1:8001/student/',
    }).then((response)=>{
      const data = response.data
      setStudents(data)
      console.log(response.data)
    }).catch((error)=>{
      if (error.response){
        console.log(error.response);
        console.log(error.response.status);
        console.log(error.response.headers);
      }
    })
  }
  
  function createStudent(event){
    axios({
      method:'POST',
      url:'http://127.0.0.1:8001/student-create/',
      data: {
        name:studentForm.name,
        age:studentForm.age
      }
    }).then((response)=>{
      getAllStudents()
    })
    setStudentForm(({
      name:"",
      age:0
    }))
    event.preventDefault();
  }

  const handleOnChange = (e) => {
    const { name, value } = e.target
    setStudentForm({ ...studentForm, [name]: value });
  };

  return (
    <div className="App">
      <h1>Hello</h1>
      <form className='create-student'>
        <input onChange={handleOnChange} name="name" placeholder='input name' value={studentForm.name}></input>
        <input onChange={handleOnChange} name="age" placeholder='input age' value={studentForm.age} type="number"></input>
        <button onClick={createStudent}>create</button>
      </form>
      { 
        
        students.map((student, i)=>{
          return (
            <h2 key={student.id}>{student.name} {student.age}</h2>
          )
        })
      }
    </div>
  );
}

export default App;
